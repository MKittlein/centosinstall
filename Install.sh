#!/bin/bash

##----------VARIABLES----------##
TOMCAT_URL="https://apache.dattatec.com/tomcat/tomcat-9/v9.0.46/bin/apache-tomcat-9.0.46.tar.gz"
TOMCAT_VERSION="9.0.46"
TOMCAT_DIR="/opt/Tomcat"
CATALINA_OPTS="'-Xms512M -Xmx1024M -server -XX:+UseParallelGC'"


##----------Actualizando instalación----------##
echo "Actualizando SO..."
sudo yum -y update |& tee repo_log.txt

##----------SSH----------##
echo "Instalando SSH..."
sudo yum install -y openssh-server openssh-clients
sudo systemctl start sshd
sudo systemctl enable sshd

##----------JAVA----------##
echo "instalando Java"
sudo yum install -y java-11-openjdk java-11-openjdk-devel dnf-plugins-core


#Set de JAVA_HOME en caso de que haya más de un JDK instalado, No estoy seguro de como resuelve eso pero funciona bien y parece ser la solucion standard
export JAVA_HOME=$(dirname $(dirname $(readlink $(readlink $(which javac)))))

sudo rm -f /etc/profile.d/java.sh #forzamos el borrado del archivo en caso de que se haya corrido el script más de una vez
#Agregamos el import de javahome para que sobreviva entre reinicios
sudo touch /etc/profile.d/java.sh
sudo sh -c "echo 'export JAVA_HOME=$(dirname $(dirname $(readlink $(readlink $(which javac)))))' >> /etc/profile.d/java.sh"


function check_java_home {
    if [ -z "${JAVA_HOME}" ]
    then
        echo "No se pudo crear JAVA_HOME reiniciar y volver a correr el script"
	exit
    else
	echo "JAVA_HOME: ""$JAVA_HOME"
        if [ ! -e "${JAVA_HOME}" ]
        then
	    echo "JAVA_HOME inválido. Algo salió mal..."
	    exit
        fi
    fi
}


##----------TOMCAT----------##
echo "Instalando tomcat server..."
echo "Checking for JAVA_HOME..."
check_java_home

echo "Descargando tomcat 9..."
# shellcheck disable=SC2144
if [ ! -f /etc/apache-tomcat-9*tar.gz ]
then
    curl -O $TOMCAT_URL
fi
echo "Descarga de Tomcat finalizada..."

echo "Creando directorios.."
sudo mkdir -p "$TOMCAT_DIR-$TOMCAT_VERSION"

if [ -d "$TOMCAT_DIR-$TOMCAT_VERSION" ]
then
    echo "Extrayendo directorio..."
    sudo tar xzf apache-tomcat-9*tar.gz -C "$TOMCAT_DIR-$TOMCAT_VERSION" --strip-components=1
    echo "Creando grupos..."
    sudo groupadd tomcat
    sudo useradd -s /bin/false -g tomcat -d $TOMCAT_DIR-$TOMCAT_VERSION tomcat

    echo "Dando permisos..."
    cd "$TOMCAT_DIR-$TOMCAT_VERSION"
    sudo chgrp -R tomcat "$TOMCAT_DIR-$TOMCAT_VERSION"
    sudo chmod -R g+r conf
    sudo chmod -R g+x conf

    sudo chmod -R g+w conf

    sudo chown -R tomcat webapps/ work/ temp/ logs/

    echo "Setting up tomcat service..."
    sudo touch tomcat.service
    sudo chmod 777 tomcat.service
    echo "[Unit]" > tomcat.service
    echo "Description=Apache Tomcat Web Application Container" >> tomcat.service
    echo "After=network.target" >> tomcat.service

    echo "[Service]" >> tomcat.service
    echo "Type=forking" >> tomcat.service

    echo "Environment=JAVA_HOME=$JAVA_HOME" >> tomcat.service
    echo "Environment=CATALINA_PID=$TOMCAT_DIR-$TOMCAT_VERSION/temp/tomcat.pid" >> tomcat.service
    echo "Environment=CATALINA_HOME=$TOMCAT_DIR-$TOMCAT_VERSION" >> tomcat.service
    echo "Environment=CATALINA_BASE=$TOMCAT_DIR-$TOMCAT_VERSION" >> tomcat.service
    echo "Environment=CATALINA_OPTS=$CATALINA_OPTS" >> tomcat.service
    echo "Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'" >> tomcat.service

    echo "ExecStart=$TOMCAT_DIR-$TOMCAT_VERSION/bin/startup.sh" >> tomcat.service
    echo "ExecStop=$TOMCAT_DIR-$TOMCAT_VERSION/bin/shutdown.sh" >> tomcat.service

    echo "User=tomcat" >> tomcat.service
    echo "Group=tomcat" >> tomcat.service
    echo "UMask=0007" >> tomcat.service
    echo "RestartSec=10" >> tomcat.service
    echo "Restart=always" >> tomcat.service

    echo "[Install]" >> tomcat.service
    echo "WantedBy=multi-user.target" >> tomcat.service

    #Inserta la configuración adecuada en web.xml en el directorio de configuración de tomcat (por defecto ya tiene los welcome files correctos por lo que solos e agregan error pages y error codes)
	  sudo sh -c "sed -i '/^<\/web-app>.*/i \\\t<error-page>\n\t\t<error-code>404<\/error-code>\n\t\t<location>\/index.html<\/location>\n\t<\/error-page>' $TOMCAT_DIR-$TOMCAT_VERSION/conf/web.xml"
    sudo mv tomcat.service /etc/systemd/system/tomcat.service
    sudo chmod 755 /etc/systemd/system/tomcat.service
    sudo systemctl daemon-reload
    echo "Starting tomcat server...."
    sudo systemctl start tomcat
    echo "Tomcat instalado"
else
    echo "No se pudo encontrar el directorio de instalación"
    exit
fi

if [ ! -f apache-tomcat-$TOMCAT_VERSION.tar.gz ]
then
    sudo rm -r apache-tomcat-$TOMCAT_VERSION.tar.gz
fi
sudo yum -y install httpd

##----------POSTGRESQL----------##
# Agregamos el repositorio actualizado de postgre:
sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
# Deshabilitamos el repositorio viejo:
sudo dnf -qy module disable postgresql

# Instalando PostgreSQL:
sudo dnf install -y postgresql13-server

# Iniciamos la base de dato y habilitamos el servicio:
sudo /usr/pgsql-13/bin/postgresql-13-setup initdb
sudo systemctl enable postgresql-13
sudo systemctl start postgresql-13

##----------POSTGIS----------##




sudo dnf -qy module disable postgresql
sudo dnf -y install epel-release dnf-plugins-core
sudo dnf config-manager --set-enabled powertools

sudo dnf -y install postgis31_13
sudo -i -u postgres
psql -c "CREATE EXTENSION postgis;"
#Checkear que la extensión se haya instaldo correctamente
psql -c "SELECT * FROM pg_available_extensions"
#Checkear que la extensión se haya instaldo correctamente

#En caso de que haya problemas de dependendencias entre los repositorios
#Herramientas necesarias para construir postgis en caso de que haya que volver a compilarla desde el source
#sudo dnf install -y proj-devel jasper-devel gcc-c++

#git clone https://github.com/OSGeo/gdal
#cd /gdal/gdal
#./configure
#make
#wget  http://mirror.centos.org/centos/8/AppStream/x86_64/os/Packages/poppler-0.66.0-27.el8.i686.rpm
#sudo dnf localinstall -y poppler-0.66.0-27.el8.i686.rpm
#sudo dnf -y install postgis31_13
